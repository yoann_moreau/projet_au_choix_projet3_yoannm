<?php
$title = 'Edition d\'article';
include 'include/header.php';
?>

<main id="edit">
  <h2>Edition d'article</h2>
  <form id="write-article" method="post"
    action="php/editArticle.php?id=<?php getId(); ?>">
    <h4>Catégories :</h4>
    <?php
      edit();
    ?>
    <input type="submit" name="write-article" value="Envoyer">
  </form>
  <form id="delete-article" method="post"
    action="php/deleteArticle.php?id=<?php getId(); ?>">
    <input type="submit" name="delete-article" class="delete"
      value="Supprimer l'article">
  </form>
</main>

<section id="overlay">
  <article class="window">
    <div class="close">
      <i class="fas fa-times"></i>
    </div>
    <h3>Erreur</h3>
    <p id="errors"></p>
  </article>
</section>

<script type="text/javascript" src="public/js/validateForm.js"></script>

<?php
include 'include/footer.php';
?>

<?php
$title = 'Ecrire un article';
include 'include/header.php';
?>

<main id="write">
  <h2>Article</h2>
  <form id="write-article" method="post" action="php/writeArticle.php">
    <h4>Catégories :</h4>
    <div class="flex wrap checkboxes">
      <div class=checkbox>
        <input type="checkbox" name="category[]" id="cb-game" value="Jeu">
        <label for="cb-game">Jeu</label>
      </div>
      <div class="checkbox">
        <input type="checkbox" name="category[]" id="cb-prog" value="Programmation">
        <label for="cb-prog">Programmation</label>
      </div>
      <div class=checkbox>
        <input type="checkbox" name="category[]" id="cb-web" value="Web">
        <label for="cb-web">Web</label>
      </div>
      <div class=checkbox>
        <input type="checkbox" name="category[]" id="cb-irl" value="IRL">
        <label for="cb-irl">IRL</label>
      </div>
    </div>
    <h4>Titre :</h4>
    <input type="text" name="title" placeholder="Titre de l'article">
    <h4>Article :</h4>
    <textarea name="text" placeholder="Votre article ici..."></textarea>
    <input type="submit" name="write-article" value="Envoyer">
  </form>
</main>

<section id="overlay">
  <article class="window">
    <div class="close">
      <i class="fas fa-times"></i>
    </div>
    <h3>Erreur</h3>
    <p id="errors"></p>
  </article>
</section>

<script type="text/javascript" src="public/js/validateForm.js"></script>

<?php
include 'include/footer.php';
?>

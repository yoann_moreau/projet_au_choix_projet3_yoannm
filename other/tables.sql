#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: categories
#------------------------------------------------------------

CREATE TABLE categories(
        id_category   Int  Auto_increment  NOT NULL ,
        category_name Varchar (250) NOT NULL
	,CONSTRAINT categories_PK PRIMARY KEY (id_category)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: authors
#------------------------------------------------------------

CREATE TABLE authors(
        id_author    Int  Auto_increment  NOT NULL ,
        username     Varchar (50) NOT NULL ,
        password     Varchar (250) NOT NULL ,
        display_name Varchar (50) NOT NULL
	,CONSTRAINT authors_PK PRIMARY KEY (id_author)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: articles
#------------------------------------------------------------

CREATE TABLE articles(
        id_article   Int  Auto_increment  NOT NULL ,
        title        Varchar (150) NOT NULL ,
        article_text Text NOT NULL ,
        date         Datetime NOT NULL ,
        picture_name Varchar (50) NOT NULL ,
        id_author    Int NOT NULL
	,CONSTRAINT articles_PK PRIMARY KEY (id_article)

	,CONSTRAINT articles_authors_FK FOREIGN KEY (id_author) REFERENCES authors(id_author)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: have_category
#------------------------------------------------------------

CREATE TABLE have_category(
        id_category Int NOT NULL ,
        id_article  Int NOT NULL
	,CONSTRAINT have_category_PK PRIMARY KEY (id_category,id_article)

	,CONSTRAINT have_category_categories_FK FOREIGN KEY (id_category) REFERENCES categories(id_category)
	,CONSTRAINT have_category_articles0_FK FOREIGN KEY (id_article) REFERENCES articles(id_article)
)ENGINE=InnoDB;


function gid(element) {
  return document.getElementById(element);
}

gid('choose-sign-in').addEventListener('click', function() {
  gid('choose-sign-in').classList.remove('inactive');
  gid('choose-sign-up').classList.add('inactive');
  gid('sign-in').classList.remove('hidden');
  gid('sign-up').classList.add('hidden');
});

gid('choose-sign-up').addEventListener('click', function() {
  gid('choose-sign-up').classList.remove('inactive');
  gid('choose-sign-in').classList.add('inactive');
  gid('sign-up').classList.remove('hidden');
  gid('sign-in').classList.add('hidden');
});

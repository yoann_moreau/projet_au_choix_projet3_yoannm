<nav>
  <a href="/blog/write.php" class="write" title="Ecrire un article">
    <i class="fas fa-edit"></i>
    <span class="large">Ecrire un article</span>
  </a>
  <a href="/blog/user.php" class="user" title="Mes articles">
    <i class="fas fa-user"></i>
    <?php
      echo $_SESSION['user'];
    ?>
  </a>
  <a href="/blog/logout.php" class="logout" title="Se déconnecter">
    <i class="fas fa-sign-out-alt"></i>
    <span class="large">Se déconnecter</span>
  </a>
</nav>
